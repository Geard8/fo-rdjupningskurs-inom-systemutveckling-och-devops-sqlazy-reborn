# DevOps Environment With Observability
This project aims to set up a DevOps environment with observability using **Prometheus** and **Grafana** using **Docker Compose**. The purpose of this project is to learn how to set up a DevOps environment using a **git server**, **CI/CD** pipelines, Docker Registry, etc. This project has been developed within our group according to the **Agile Scrum** model.

# Project team members
As mentioned above, this is a group project, and here are our contributors:
- Daniel Goldman Lapington
- Daniel Renner
- Erik J Olsson
- Nicklas Thor
- Ninos Zaiya
- Sara Törnström
- Selim Hussen

## The project is made up of two parts:
 1. ### Set up a DevOps environment:
    Set up a **DevOps** environment with observability **Prometheus/Grafana** locally using **Docker** and **Compose**.
    The environment is made up of **OneDev** (used for version control as well as **CI/CD**) and a **selfhosted docker registry** used for delivery.
    
 2. ### Custom OneDev prometheus exporter:
    A custom exporter for **OneDev** build data is to be written in **Python** using the official **Prometheus** client library.
    The repository should be public and **OneDev CI/CD pipelines** should be set up on the local instance for **test** and **prod** (delivery).
    Make sure to include your **OneDev** pipeline files in the **public GitLab repo**.  

# How to set up DevOps Environment?

## 1. Set up a DevOps environment
  - Clone this repository ```git clone https://gitlab.com/Geard8/fo-rdjupningskurs-inom-systemutveckling-och-devops-sqlazy-reborn.git```
  - ```cd fo-rdjupningskurs-inom-systemutveckling-och-devops-sqlazy-reborn```
  - ```hostname -I``` or ```ip a``` to get your **IP address**.
  - Open ```prometheus.yml``` with your favorite text editor (In my case vim ```vim prometheus.yml```), scroll down to ```static_configs``` and change ```YOUR.IP.ADDRESS.HERE:8000 and change the``` to your **IP adress**.
  - Run ```sudo docker compose up -d``` to start all the docker containers and make sure is everything is running.
  - Make sure that all your docker containers are up by running ```sudo docker compose ps```, as well as visiting these links:
    - **Prometheus**:     http://localhost:9090
    - **Grafana**:        http://localhost:3000
    - **Registry**:       http://localhost:5000/v2/_catalog
    - **OneDev**:         http://localhost:6610
  
## 2a. Custom OneDev prometheus exporter:
  - Go to **OneDev**:http://localhost:6610) and set up an account. Make sure that you save your **credentials** so you don't forget them.
  - Go to projects and make a new project. Name it "prom_expo".
  - Copy the ```OneDev_Repo``` folder and place it in a chosen location ```cp -R ./OneDev_Repo /your/chosen/path/OneDev_Repo```.
  - Go to the copied OneDev_Repo ```cd /your/chosen/path/OneDev_Repo```.
  - Go to ```prometheus_exporter``` (```cd ./prometheus_exporter```) and rename the ```template_config.py``` to ```config.py``` . Once that's done, enter your login credentials for OneDev. OneDev uses email as username, so make sure to fill that in when entering your credentials. Also, make sure to use your **IP address** when filling in the onedev_URL . It should look something like this: ```onedev_URL = "http://YOUR-IP:6610"```.
  - Go back to OneDev_Repo```cd ..``` 
  - Run ```git init```.
  - Then run ```git add .; git commit -m "message"; git remote add origin http://localhost:6610/prom_expo; git push -u origin master``` (If you run into ```*** Please tell me who you are.```, then fill in the commands it shows and try again).
  - Now you should see that a pipeline has started in onedev (http://localhost:6610). Once that's done, return to the original git repo (```cd git/repo/path/```) and run the bash script file located there ```./run_exporter.sh```.

## 2b. Setup Grafana:
  - Go to **Grafana**:        http://localhost:3000
  - Sign in as admin (use admin as both username and password).
  - Create new username and password.
  - To select a datasource click on Configurations -> Data sources.
  - Click on add data source.
  - Select **Prometheus**.
  - Type in the **IP** for **Prometheus** in the url field, like this ```http://prometheus:9090```.
  - Click on Save & test.
  - Click on New dashboard under the Dashboards icon in the side menu.
  - Add a new panel.
  - Add metric -> projects_created_in_2022 -> Run queries -> Click Save (top right corner).
  - Choose a name for the dashboard and then click save.

## Git branching strategy
We plan to start out working on the main branch together until we have the foundation, e.g. d Docker, Compose file. And then along the way we might discover more what we can do together.

Later we'll start using branches to develop separate "features", that when functioning can be merged into the main branch.

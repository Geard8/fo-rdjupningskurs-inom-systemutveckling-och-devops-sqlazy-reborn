'''
exporter.py is used as an exporter for prometheus
to scrape data from onedev by makeing use of api call to onedev
'''

from prometheus_client import start_http_server, Gauge
from .onedev_api import api_call

parameters = {
    "count": 100,
    "offset": 0,
    }
links_api = {
    "projects":"api/projects",
    "issues":"api/issues",
    "builds": "api/builds",
    "pull_requests":"api/pull-requests"
}

def get_issues_state():
    ''' make api call to find out and return how many issues is closed'''
    response = api_call(links_api["issues"], params=parameters)
    if response.status_code == 200:
        json_response = response.json()
        counting = 0
        for i in json_response:
            if i["state"] == "Closed":
                counting += 1
        return counting
    return 0

def get_pull_request_state():
    ''' make api call to find out and return how many pull request is discarded'''
    response = api_call(links_api["pull_requests"], params=parameters)
    if response.status_code == 200:
        json_response = response.json()
        counting = 0
        for i in json_response:
            if i["status"] == "DISCARDED":
                counting += 1
        return counting
    return 0

def projects_created_in_2022():
    ''' make api call to find out and return how many projects is made in 2022'''
    response = api_call(links_api["projects"], params=parameters)
    if response.status_code == 200:
        json_response = response.json()
        counting = 0
        for i in json_response:
            if i["createDate"][0:4] == "2022":
                counting += 1
        return counting
    return 0
if __name__ == "__main__":
    start_http_server(8000)
    status_issus_closed = Gauge("issuse_closed", "The Total issuses was closed")
    status_issus_closed.set_function(get_issues_state)
    pull_request_discarded = Gauge("pull_request_discarded", "Pull request was discarded")
    pull_request_discarded.set_function(get_pull_request_state)
    projects_in_2022 = Gauge("projects_created_in_2022", "Total Projects That Created in 2022")
    projects_in_2022.set_function(projects_created_in_2022)
    while True:
        pass

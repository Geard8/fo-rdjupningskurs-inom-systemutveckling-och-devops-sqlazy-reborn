from prometheus_exporter.config import onedev_username, onedev_password, onedev_URL

import requests

def api_call(call_path, params = None):
    """
    Make api call to onedev based on call_path and return response.
    response is from using requests.get to call onedev api.

    param: 
        call_path: str that is path part of the URL to call onedev api
            example "api/projects/1" to call onedev api to get project with id 1.
            onedev api path can be found at: https://code.onedev.io/help/api
        params: dict with params for api call
    return: response: object from using requests.get to onedev api.
    """

    if params:
        response = requests.get(f'{onedev_URL}/{call_path}', auth=(onedev_username, onedev_password), params=params)
    else:
        response = requests.get(f'{onedev_URL}/{call_path}', auth=(onedev_username, onedev_password))

    return response

from prometheus_exporter.onedev_api import api_call
import pytest


def test_api_call():
    response = api_call("api/projects/1")
    assert response.status_code == 200 or response.status_code == 400

from prometheus_exporter.onedev_api import api_call
from prometheus_exporter.exporter import get_issues_state, get_pull_request_state,projects_created_in_2022
import pytest

parameters = {
    "count": 100,
    "offset": 0,
    }
links_api = {"projects":"api/projects",
               "issues":"api/issues",
               "builds": "api/builds",
               "pull_requests":"api/pull-requests"
               }

def test_get_issues_state():
    response = api_call(links_api["issues"], params=parameters)
    assert response.status_code == 200 
    retVal = get_issues_state()
    assert type(retVal) is int  

def test_get_pull_request_state():
    response = api_call(links_api["pull_requests"], params=parameters)
    assert response.status_code == 200 
    retVal = get_pull_request_state()
    assert type(retVal) is int 

def test_projects_created_in_2022():
    response = api_call(links_api["projects"], params=parameters)
    assert response.status_code == 200 
    retVal = projects_created_in_2022()
    assert type(retVal) is int 


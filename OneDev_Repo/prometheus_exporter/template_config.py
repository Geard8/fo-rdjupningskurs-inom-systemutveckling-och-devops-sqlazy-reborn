# This is a template for config.py that can be used by making a copy of it and rename the copy to config.py
# Then with your config.py you can change the values to what you want it to be.

# replace REPLACE with correct login credentials for local onedev
onedev_username = "REPLACE"
onedev_password = "REPLACE"

# replace "http://localhost:6610" with URL to local onedev
onedev_URL = "http://localhost:6610" # example "http://localhost:6610 change if needed"

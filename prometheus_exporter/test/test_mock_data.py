import os
import re

from ..exporter import parameters, links_api, get_issues_state, get_pull_request_state, projects_created_in_2022
from prometheus_exporter.config import onedev_username, onedev_password, onedev_URL
import pytest
import requests
import json

parameters = parameters
links_api = links_api


def test_issues_api(requests_mock):
    #open text file in read mode
    with open(f"{os.path.dirname(os.path.abspath(__file__))}/test_data/issues.json", "r") as text_file:
        #read whole file to a string    
        json_string = text_file.read()   


    requests_mock.get(f'{onedev_URL}/{links_api["issues"]}', json=json.loads(json_string))
    count = get_issues_state()
    
    assert count == 2

def test_pull_api(requests_mock):
    requests_mock.get(f'{onedev_URL}/{links_api["pull_requests"]}', json=[])
    count = get_pull_request_state()
    
    assert count == 0

def test_projects_2022_api(requests_mock):
    #open text file in read mode
    with open(f"{os.path.dirname(os.path.abspath(__file__))}/test_data/projects.json", "r") as text_file:
        #read whole file to a string    
        json_string = text_file.read()   


    requests_mock.get(f'{onedev_URL}/{links_api["projects"]}', json=json.loads(json_string))
    count = projects_created_in_2022()
    
    assert count == 5
